package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {

		MyTime myTime = new MyTime(22,59,59);
		
		System.out.println(myTime.toString());
		myTime.nextSecond();
		System.out.println(myTime.toString());
		
		MyTime myTime2 = new MyTime(22,0,0);
		
		System.out.println(myTime2.toString());
		myTime2.previousSecond();
		System.out.println(myTime2.toString());
		
	}

}
