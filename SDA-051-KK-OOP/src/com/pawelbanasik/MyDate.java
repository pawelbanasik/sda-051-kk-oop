package com.pawelbanasik;

public class MyDate {

	private int year;
	private int month;
	private int day;

	String[] strMonths = { "Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	String[] strDays = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
	int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public MyDate(int year, int month, int day) {
		super();
		this.year = year;
		this.month = month;
		this.day = day;

	}
	
	public void setDate(int year, int month, int day){
		this.day = day;
		this.month = month;
		this.year = year;
		
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

//	public boolean isLeapYear(int year) {
//
//		 
//	}
//	public boolean isValidDate(int year, int month, int day){
//		
//	}
//	
//	public int getDayOfWeek(int year, int month, int day){
//		
//	}

	@Override
	public String toString() {
		return "MyDate [year=" + year + ", month=" + month + ", day=" + day + "]";
	}
	
	
	
}
