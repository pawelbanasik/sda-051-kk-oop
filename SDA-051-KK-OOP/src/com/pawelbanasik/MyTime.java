package com.pawelbanasik;

public class MyTime {

	private int hour = 0;
	private int minute = 0;
	private int second = 0;

	public MyTime() {
		super();
	}

	public MyTime(int hour, int minute, int second) {
		super();
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	@Override
	public String toString() {
		String time = "";
		String hourString = addLeadingZeros(this.hour);
		String minuteString = addLeadingZeros(this.minute);
		String secondString = addLeadingZeros(this.second);

		time = hourString + ":" + minuteString + ":" + secondString;
		return time;
	}

	public String addLeadingZeros(int number) {
		String result = "";
		if (number < 10) {
			result = "0" + number;
		} else {
			result = Integer.toString(number);
		}
		return result;

	}

	public MyTime nextSecond() {
		this.second++;
		if (second == 60) {
			second = 0;
			nextMinute();
		}

		return this;
	}

	public MyTime nextMinute() {
		this.minute++;
		if (minute == 60) {
			minute = 0;
			nextHour();
		}

		return this;

	}

	public MyTime nextHour() {

		this.hour++;
		this.hour = hour % 24;

		return this;

	}

	public MyTime previousHour() {
		this.hour--;

		return this;
	}

	public MyTime previousMinute() {
		this.minute--;
		if (minute == -1) {
			minute = 59;
			previousHour();
		}
		return this;
	}

	public MyTime previousSecond() {
		this.second--;
		if (second == -1) {
			second = 59;
			previousMinute();
		}
		return this;
	}

}
